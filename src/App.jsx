import React from 'react';
import CursorPaper, {CursorContext} from './components/CursorPaper'

export const App = () => {
    return (
        <CursorPaper>
            <Block />
            <CursorView />
        </CursorPaper>
    );
}

export const CursorView = () => {
    const [,, cursorRef, canvasRef] = React.useContext(CursorContext)
    return (
        <div>
            <div ref={cursorRef} className="cursor cursor--small"></div>
            <canvas ref={canvasRef} className="cursor cursor--canvas" resize='true'></canvas>
        </div>
    )
}

export const Block = () => {
    const [hoverCursorPlus, hoverCursorStandart ,,] = React.useContext(CursorContext)
    return (
        <div className="block" onMouseEnter={hoverCursorPlus} onMouseLeave={hoverCursorStandart}>
        </div>
    )
}
