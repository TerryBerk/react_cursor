import React from 'react'
import paper from 'paper'
import SimplexNoise from 'simplex-noise'

export default class CursorPaper extends React.Component {
    constructor(props) {
        super(props)

        this.state = {lastX: 100, lastY: 100, x: 100, y: 100}

        this.block = React.createRef()
        this.polygon = 0
        this.noiseScale = 150
        this.noiseRange = 4
        this.bigCoordinates = []

        this.cursor = React.createRef()
        this.canvas = React.createRef()

    }

    lerp = (a, b, n) => (1 - n) * a + n * b
    componentDidMount() {
        document.addEventListener('mousemove', event => this.setState({x: event.clientX, y: event.clientY}))

        this.renderCursor()
        this.renderPolygon()
    }

    enter = () => {
      this.polygon.scale(5)
      this.setState({
          isNoize: true
      });

    }
    leave = () => {
      this.polygon.scale(0.2)
      this.setState({
          isNoize: false
      });
    }


    renderCursor = () => {
        this.cursor.current.style.transform = `translate(${this.state.x}px, ${this.state.y}px)`
        requestAnimationFrame(this.renderCursor)
    }

    renderPolygon = () => {
        paper.setup(this.canvas.current)
        this.polygon = new paper.Path.RegularPolygon(new paper.Point(this.state.x, this.state.y), 8, 10)

        this.polygon.strokeColor = 'rgba(255, 0, 0, 0.5)'
        this.polygon.strokeWidth = 2
        this.polygon.smooth()

        const group = new paper.Group([this.polygon])
        group.applyMatrix = false

        const noiseObjects = this.polygon.segments.map(() => new SimplexNoise());
        console.log("noiseObjects", noiseObjects);

        this.animation(group, noiseObjects)

    }

    map = (value, in_min, in_max, out_min, out_max) => {
        return (
          ((value - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min
          );
        };


    animation = (group, noiseObjects) => {
        paper.view.onFrame = event => {

            this.setState({
                lastX: this.lerp(this.state.lastX, this.state.x, 0.2),
                lastY: this.lerp(this.state.lastY, this.state.y, 0.2)
            })

            group.position = new paper.Point(this.state.lastX, this.state.lastY)

            // Noise

            if (this.state.isNoize) {
                console.log("noizy")

                if (this.bigCoordinates.length === 0) {
                  this.polygon.segments.forEach((segment, i) => {
                    this.bigCoordinates[i] = [segment.point.x, segment.point.y];
                  });
                }

                if (event.count > 1)
                this.polygon.segments.forEach((segment, i) => {
                    const noiseX = noiseObjects[i].noise2D(event.count / this.noiseScale, 0);
                    const noiseY = noiseObjects[i].noise2D(event.count / this.noiseScale, 1);

                    const distortionX = this.map(noiseX, -1, 1, -this.noiseRange, this.noiseRange);
                    const distortionY = this.map(noiseY, -1, 1, -this.noiseRange, this.noiseRange);

                    const newX = this.bigCoordinates[i][0] + distortionX;
                    const newY = this.bigCoordinates[i][1] + distortionY;
                    segment.point.set(newX, newY);
                  });
              }
        }
    }

    render = () => {
        return (
            <CursorProvider
              cursorRef={this.cursor}
              canvasRef={this.canvas}
              enter={this.enter}
              leave={this.leave}
            >
                {this.props.children}
            </CursorProvider>
        )
    }
}

export const CursorContext = React.createContext(null)

export const CursorProvider = ({ children, enter, leave, cursorRef, canvasRef }) => {
  // const [isSize, setSize] = React.useState(false);

  const hoverCursorPlus = () => {
    // setSize(true);
    enter();
  }

  const hoverCursorStandart = () => {
    // setSize(false);
    leave();
  }

  return (
      <CursorContext.Provider value={[hoverCursorPlus, hoverCursorStandart, cursorRef, canvasRef]}>
          {children}
      </CursorContext.Provider>
  );
}

// export const CursorPaper = () => {
//
//     const cursor = React.useRef()
//     const canvas = React.useRef()
//
//     const [state, setState] = React.useState({
//         clientX: 100,
//         clientY: 100,
//         lastX: 100,
//         lastY: 100
//     })
//
//     React.useEffect(() => {
//         document.addEventListener('mousemove', function(event) {
//             setState({
//                 clientX: event.clientX,
//                 clientY: event.clientY
//             })
//         })
//         renderCursor()
//
//         setup(canvas.current)
//         renderPolygon()
//
//         renderCursor()
//     })
//
//
//     function renderCursor() {
//         cursor.current.style.transform = `translate(${state.clientX}px, ${state.clientY}px)`
//         requestAnimationFrame(renderCursor);
//     }
//
//     const renderPolygon = () => {
//         const polygon = new paper.Path.RegularPolygon(new paper.Point(state.clientX, state.clientY), 8, 10)
//
//         polygon.strokeColor = 'rgba(255, 0, 0, 0.5)'
//         polygon.strokeWidth = 2
//         polygon.smooth()
//
//         const group = new paper.Group([polygon])
//         group.applyMatrix = false
//
//         // renderNoise(group)
//     }
//
//     // Render Noise
//     // const renderNoise = (group) => {
//     //     polygon.segments.map(() => new SimplexNoise());
//     //     animation(group)
//     // }
//     //
//     // // requestAnimationFrame
//     // animation = (group) => {
//     //     paper.view.onFrame = event => {
//     //
//     //         const {clientY, clientX, lastX, lastY} = state
//     //
//     //         setState({
//     //             lastX: lerp(lastX, clientX, 0.2),
//     //             lastY: lerp(lastY, clientY, 0.2)
//     //         })
//     //
//     //         group.position = new paper.Point(lastX, lastY)
//     //     }
//     // }
//
//     return (
//         <div>
//             <div className="cursor cursor--small" ref={cursor}></div>
//             <canvas ref={canvas} className="cursor cursor--canvas" resize='true'></canvas>
//         </div>
//     )
// }
